package com.skcraft.launcher.launch;

import com.skcraft.concurrency.SettableProgress;
import com.skcraft.launcher.util.Environment;
import lombok.Getter;
import lombok.extern.java.Log;

import java.io.File;
import java.util.logging.Level;

/**
 * @author dags <dags@dags.me>
 */
@Log
public abstract class JREFinder {

    @Getter
    private final SettableProgress progress;

    public JREFinder(SettableProgress progress) {
        this.progress = progress;
    }

    public abstract File getRuntimeDir();

    public abstract void installJRE();

    public File findJRE() {
        File runtime = getRuntimeDir();
        if (runtime == null) {
            log.log(Level.INFO, "Could not detect runtime dir on platform {0}", Environment.getInstance().getPlatform());
            return null;
        }

        log.log(Level.INFO, "Detected runtime dir: {0}, exists: {1}", new Object[]{runtime, runtime.exists()});

        File x64 = resolve(runtime, "jre-x64");
        File jre = getLatestJRE(x64);
        if (jre == null) {
            installJRE();
        }

        jre = getLatestJRE(x64);
        if (jre == null) {
            log.log(Level.INFO, "Could not locate JRE in: {0}, exists: {1}", new Object[]{x64, x64.exists()});
            return null;
        }

        log.log(Level.INFO, "Detected JRE: {0}", jre);

        if (!resolve(jre, "bin", "java").setExecutable(true)) {
            log.log(Level.WARNING, "Unable to make 'java' executable!");
        }

        if (!resolve(jre, "bin", "javaw").setExecutable(true)) {
            log.log(Level.WARNING, "Unable to make 'javaw' executable!");
        }

        return resolve(jre, "bin");
    }

    public File getLatestJRE(File dir) {
        File[] jres = dir.listFiles();
        if (jres == null || jres.length == 0) {
            return null;
        }

        File latest = null;
        for (File jre : jres) {
            if (latest == null || jre.lastModified() > latest.lastModified()) {
                latest = jre;
            }
        }

        return latest;
    }

    public File resolve(File parent, String... path) {
        File file = parent;
        for (String s : path) {
            file = new File(file, s);
        }
        return file;
    }
}
