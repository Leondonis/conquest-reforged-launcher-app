package com.skcraft.launcher.launch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skcraft.concurrency.SettableProgress;
import com.skcraft.launcher.util.Environment;
import com.skcraft.launcher.util.Platform;
import com.skcraft.launcher.util.SharedLocale;
import lombok.extern.java.Log;
import org.apache.commons.compress.compressors.lzma.LZMACompressorInputStream;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * @author dags <dags@dags.me>
 * Downloads the Mojang 64bit runtime
 */
@Log
public class LocalJRE extends JREFinder {

    private static final String launcher_data = "http://launchermeta.mojang.com/mc-staging/launcher.json";

    public LocalJRE(SettableProgress progress) {
        super(progress);
    }

    @Override
    public File getRuntimeDir() {
        return resolve(new File("").getAbsoluteFile(), "runtime");
    }

    @Override
    public void installJRE() {
        try {
            File x64 = resolve(getRuntimeDir(), "jre-x64");

            JsonNode jreMeta = getJRE();
            if (jreMeta == null) {
                return;
            }

            // download the jre to the version dir
            // decompress from lzma -> zip
            // extract zip to version dir

            String jreURL = jreMeta.get("url").asText();
            String jreVersion = jreMeta.get("version").asText();

            File jreDir = resolve(x64, jreVersion);
            File zip = new File(jreDir, jreVersion + ".zip");
            File lzma = new File(jreDir, jreVersion + ".lzma");

            download(jreURL, lzma);
            decompress(lzma, zip);
            extract(zip, zip.getParentFile());

            if (lzma.delete()) {
                log.log(Level.INFO, "Removing installation file: {0}", lzma);
            }

            if (zip.delete()) {
                log.log(Level.INFO, "Removing installation file: {0}", zip);
            }
        } catch (Throwable ignore) {
            log.log(Level.WARNING, "Something went wrong whilst try to install the jre locally");
        }
    }

    private JsonNode getJRE() {
        Platform system = Environment.getInstance().getPlatform();
        String os = system == Platform.WINDOWS ? "windows" : system == Platform.MAC_OS_X ? "osx" : "linux";

        if (os.equals("linux")) {
            throw new UnsupportedOperationException("Mojang do not support a linux JRE :[");
        }

        InputStream inputStream = null;
        JsonNode jre = null;

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(launcher_data).openConnection();
            inputStream = connection.getInputStream();
            JsonNode root = new ObjectMapper().readTree(inputStream);
            JsonNode platform = root.get(os);
            JsonNode x64 = platform.get("64");
            jre = x64.get("jre");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(inputStream);
        }

        if (jre == null) {
            throw new UnsupportedOperationException("Cannot retrieve JRE url for system " + os + ":x64");
        }

        return jre;
    }

    private void download(String url, File destination) {
        if (destination.exists()) {
            log.log(Level.INFO, "Download destination already exists, skipping download: {0}", destination);
            return;
        }

        int attempts = 0;
        while (attempts++ < 5) {
            InputStream inputStream = null;

            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
                String length = connection.getHeaderField("Content-Length");
                if (length == null) {
                    log.log(Level.INFO, "Download attempt {0} failed", attempts);
                    Thread.sleep(500);
                    continue;
                }

                long len = Long.parseLong(length);
                if (len <= 0) {
                    log.log(Level.INFO, "Download attempt {0} failed", attempts);
                    Thread.sleep(500);
                    continue;
                }

                inputStream = connection.getInputStream();
                copyFile(inputStream, destination, SharedLocale.tr("localJre.download"), len);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                close(inputStream);
            }
        }
    }

    private void decompress(File lzma, File zip) {
        if (!lzma.exists()) {
            throw new UnsupportedOperationException("Attempted to decompress non-existent file: " + lzma);
        }

        if (zip.exists()) {
            log.log(Level.INFO, "Zip destination already exists, skipping decompression: {0}", zip);
            return;
        }

        BufferedInputStream inputStream = null;
        LZMACompressorInputStream lzmaInputStream = null;

        try {
            inputStream = new BufferedInputStream(new FileInputStream(lzma));
            lzmaInputStream = new LZMACompressorInputStream(inputStream);
            copyFile(lzmaInputStream, zip, SharedLocale.tr("localJre.decompress"), -1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(lzmaInputStream);
            close(inputStream);
        }
    }

    private void extract(File zip, File destination) {
        if (!zip.exists()) {
            throw new UnsupportedOperationException("Attempted to extract non-existent file: " + zip);
        }

        if (destination.mkdirs()) {
            log.log(Level.INFO, "Creating dir: {0}", destination);
        }

        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(zip);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (!entry.isDirectory()) {
                    InputStream inputStream = zipFile.getInputStream(entry);
                    File file = new File(destination, entry.getName());
                    copyFile(inputStream, file, SharedLocale.tr("localJre.extract", file.getName()), -1);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(zipFile);
        }
    }

    private void copyFile(InputStream source, File destination, String status, long length) {
        ReadableByteChannel byteChannel = null;
        FileOutputStream outputStream = null;
        try {
            if (destination.getParentFile().mkdirs()) {
                log.log(Level.INFO, "Creating dir: {0}", destination.getParentFile());
            }

            if (destination.createNewFile()) {
                log.log(Level.INFO, "Creating file: {0}", destination);
            }

            byteChannel = Channels.newChannel(source);
            outputStream = new FileOutputStream(destination);

            if (length < 0) {
                getProgress().set(status, -1);
                outputStream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            } else {
                long position = 0;
                long increment = length / 100L;
                while (position < length) {
                    outputStream.getChannel().transferFrom(byteChannel, position, increment);
                    position += increment;

                    double progress = (double) position / (double) length;
                    getProgress().set(status, progress);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(byteChannel);
            close(outputStream);
        }
    }

    private void close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
