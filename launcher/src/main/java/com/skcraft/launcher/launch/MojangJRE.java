package com.skcraft.launcher.launch;

import com.skcraft.concurrency.SettableProgress;
import com.skcraft.launcher.util.Environment;
import com.skcraft.launcher.util.Platform;

import java.io.File;

/**
 * @author dags <dags@dags.me>
 * Looks for the runtime provided by the Mojang Minecraft installation
 */
public class MojangJRE extends JREFinder {

    public MojangJRE(SettableProgress progress) {
        super(progress);
    }

    @Override
    public File getRuntimeDir() {
        if (Environment.getInstance().getPlatform() == Platform.MAC_OS_X) {
            return getMacRuntimeDir();
        }

        if (Environment.getInstance().getPlatform() == Platform.WINDOWS) {
            return getWindowsRuntimeDir();
        }

        // let's assume linux users know how to install java because fuck knows where the minecraft launcher ends up
        return null;
    }

    @Override
    public void installJRE() {
        // do nothing
    }

    // usually 'C:/Program Files (x86)/Minecraft/runtime'
    private File getWindowsRuntimeDir() {
        File mcDir = new File(System.getenv("PROGRAMFILES(X86)"), "Minecraft");
        if (!mcDir.exists()) {
            // shouldn't really ever happen but people do weird things
            mcDir = new File(System.getenv("PROGRAMFILES"), "Minecraft");
        }
        return resolve(mcDir, "runtime");
    }

    // usually '/Applications/Minecraft.app/Contents/runtime'
    // prioritise user-local applications ('~/Applications/'), fall back to global '/Applications'
    private File getMacRuntimeDir() {
        File mcApp = resolve(getHome(), "Applications", "Minecraft.app");
        if (!mcApp.exists()) {
            mcApp = new File("/Applications", "Minecraft.app"); // more likely
        }
        return resolve(mcApp, "Contents", "runtime");
    }

    private File getHome() {
        return new File(System.getProperty("user.home"));
    }
}
